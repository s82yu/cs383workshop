 /*
 * Grid of shape agents that turn or scale relative to the mouse
 * - closely based on `P_2_1_1_04` from the book "Generative Design"
 *
 */


import de.looksgood.ani.*;
import processing.video.*;
//import processing.core.*;

import gab.opencv.*;
import org.opencv.imgproc.Imgproc;

Capture cam;
OpenCV opencv;
int cam_w = 640;
int cam_h = 480;

float scale = 0.25;
float coin;
PImage output;

// list of agents
ArrayList<Fish> agents;
ArrayList<Fish> fishOutOfWater;
ArrayList<Fish> fishInPond;
Bucket bkt;
float bktBot = 42;
Fisher me;
Rod fishingRod;
Line fishingLine;
Sun sun;
// number of fish
int fishNum; 
//fish pond
float pondW,pondH,pondX,pondY;
//land
float landW,landH,landX,landY;
float scoreX,scoreY,scoreW,scoreH;
int topScore;

final float GRatio = (sqrt(5)-1)/2;
float chaos;
float timer;

int panelNum = 1;
PVector[] directions = new PVector[panelNum];
float[] baseY = new float[panelNum];
float[] moveY = new float[panelNum];
float[] baseX = new float[panelNum];
float[] moveX = new float[panelNum];


void setup() {
  
  fullScreen();
  pondW = width*GRatio;
  pondH = height*(1-GRatio);
  pondX = width-pondW;
  pondY = height-pondH;
  landW = width-pondW;
  landH = height/2;
  landX = 0;
  landY = height-landH;
  
  scoreW = width/8;
  scoreH = height/15;
  scoreX = width/2;
  scoreY = scoreH/2;
  topScore=0;
  //number of fish
  fishNum = 100;
  chaos = 1;
  timer = millis();
  
  //String[] cameras = Capture.list();
  //println("Available cameras:");
  //  for (int i = 0; i < cameras.length; i++) {
  //    println(cameras[i]);
  //  }
  noCursor();
  cam = new Capture(this, int(cam_w * scale), int(cam_h * scale));
  opencv = new OpenCV(this, cam.width, cam.height);

  cam.start();
  output = new PImage(cam.width,cam.height);
 
  for(int i =0;i < panelNum;++i){
    directions[i] = new PVector();
    baseX[i]=0;
    baseY[i]=0;
    moveX[i]=0;
    moveY[i]=0;
  }
  
  // you have to call always Ani.init() first!
  Ani.init(this);

  createAgents(fishNum);
}

boolean timerStart(){
  return millis() - timer > 60000;
}

// create the grid of agents, one agent per grid location
void createAgents(int num) {
  
  agents = new ArrayList<Fish>();
  fishOutOfWater = new ArrayList<Fish>();
  fishInPond = new ArrayList<Fish>();
  me = new Fisher();
  fishingRod = new Rod(me.bodyX,me.bodyY);
  bkt = new Bucket(landX+landW-fishingRod.rodL*3/4,landY);
  fishingLine = new Line(fishingRod.peakX,fishingRod.peakY);
  sun = new Sun();
  
  for(int i = 0;i<num;++i){
    float x=random(0,pondW);
    float y=random(0,pondH);
    Fish a = new Fish(x, y);
    agents.add(a);
  }
  
}

void reset(){
  if(bkt.caughtFish.size() > topScore){
    topScore = bkt.caughtFish.size();
  }
  background(255);
  createAgents(fishNum);
}

  void toss(){
    coin = random(0,1);
  }
  
  boolean foundFish(ArrayList<Fish> arrF,Fish f){
    for(int i=0;i < arrF.size();++i){
      if(f == arrF.get(i)){
        return true;
      }
    }
    return false;
  }
  
  void addInOut(Fish a){
    a.inWater = false;
    a.inOutArray = true;
    fishOutOfWater.add(a);
    if(foundFish(bkt.caughtFish,a)){
      if(a.freed){
        bkt.removeFish(a);
        a.inBktArray = false;
        a.inBkt = false;
      }
    }
    else if(foundFish(fishInPond,a)){
      fishInPond.remove(a);
      a.inPondArray = false;
    }
  }
  
  void addInPond(Fish a){
    a.inWater = true;
    a.inBkt = false;
    a.inPondArray = true;
    fishInPond.add(a);
    if(foundFish(bkt.caughtFish,a)){
      bkt.removeFish(a);
      a.inBktArray = false;
    }
    else if(foundFish(fishOutOfWater,a)){
      fishOutOfWater.remove(a);
      a.inOutArray = false;
    } 
  }
  
  void addInBkt(Fish a){
    a.inWater = true;
    a.inBkt = true;
    a.inBktArray = true;
    bkt.addFish(a);
    if(foundFish(fishInPond,a)){
      fishInPond.remove(a);
      a.inPondArray = false;
    }
    else if(foundFish(fishOutOfWater,a)){
      fishOutOfWater.remove(a);
      a.inOutArray = false;
    }  
  }
  
void draw() {
  if(timerStart()){
    //if(me.ready){
    //  timer = millis();
    //}
    reset();
  }
  background(50);
  if(me.ready){
    float skyR,skyB,opacity;
    skyR = map(millis()-timer,30000,60000,50,255);
    skyB = map(millis()-timer,0,30000,50,255);
    //opacity = map(millis()-timer,0,30000,0,255);
    //if(millis()-timer < 30000){
    //  background(135,206,250,200);
    //}
    //else {
    //  background(skyR,206,skyB,255);
    //}
    background(skyR,0,skyB,200);
  }
  
  
  sun.update();
  sun.draw();
  
  pushMatrix();
  ////score board
  //noFill();
  //stroke(0);
  //strokeWeight(8);
  //rect(scoreX-scoreW/2,scoreY-scoreH/2,scoreW,scoreH);
  //fill(255,0,0);
  //textSize(20);
  //text("Top score: "+topScore,scoreX-scoreW/2+10,scoreY-5);
  //fill(0);
  //text("Your score: "+bkt.caughtFish.size(),scoreX-scoreW/2+10,scoreY+20);
  
  //pond draw
  noStroke();
  fill(255);
  rect(pondX,pondY,pondW,pondH);
  fill(0,50,220,200);
  rect(pondX,pondY,pondW,pondH);
  //land draw
  fill(0);
  rect(landX,landY,landW,landH);
  popMatrix();
  
  if (cam.available() == true) {
    cam.read();

    // load frame into pipeline 
    opencv.loadImage(cam);

    // mirror
    opencv.flip(1);

    opencv.calculateOpticalFlow();
    // calculate average direction
    directions[0] = opencv.getAverageFlowInRegion(0,0,cam.width,cam.height);
    // if motion is very small, optical flow might return NaN
    for(int i = 0;i < panelNum;++i){
       if (Float.isNaN(directions[i].x) || Float.isNaN(directions[i].y)) {
          directions[i] = new PVector();
       }
    }
    // grab image for display
    //output = opencv.getSnapshot();
  }
  
  for(int i = 0;i < panelNum;i++){
    PVector a = new PVector();
    if(i == 0){
      a = new PVector(width/2, height/2);
    }
    //if(i == 1){
    //  a = new PVector(width/4, height/2);
    //}
    //if(i == 2){
    //  a = new PVector(width/4*3, height/2);
    //}
    
 
    PVector b = PVector.add(a, PVector.mult(directions[i], 200));
    moveX[i] = b.x-a.x;
    moveY[i] = b.y-a.y;
    
    if (Float.isNaN(moveX[i]) || Float.isNaN(moveY[i])) {
       println(width,height,a,directions[i]);
    }
    if(abs(moveX[i])<4){
      moveX[i] =0;
    }
    if(abs(moveY[i])<4){
      moveY[i]=0;
    }
    baseX[i] += moveX[i];
    baseY[i] += moveY[i];
    constrain(baseX[i],0,width);
    constrain(baseY[i],0,height);
    float avgMove = sqrt(sq(moveX[0])+sq(moveY[0]));
    if(avgMove>100){
      avgMove=100;
    }
    chaos = map(avgMove,0,100,0.8,3);
    //println(chaos);
    
  }
  
    me.update();
    bkt.update();
    fishingRod.update(me.bodyX,me.bodyY);
    fishingLine.update(fishingRod.peakX,fishingRod.peakY);
    
    //Fish[] removeFromPond = new Fish();
    for(int i = 0;i < agents.size();++i){
      if(agents.get(i).inWater){
        if(agents.get(i).inBkt && !agents.get(i).inBktArray){
          addInBkt(agents.get(i));
          
        }
        else if(!agents.get(i).inPondArray){
          addInPond(agents.get(i));
        }
      }
      else if(!agents.get(i).inOutArray){
        addInOut(agents.get(i));
      }
    }
    
    bkt.draw();
    for (Fish ag : agents) {
      ag.update();
      ag.draw();
    }
    
   
    me.draw();
    fishingRod.draw();
    fishingLine.draw();
    
    fill(255, 0, 0);
    text(nfc(frameRate, 1), 20, 20);
}

void keyPressed(){
  if(key == ' '){
    reset();
  }
}
