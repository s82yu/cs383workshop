class Sun{
  float x,y;
  float centerX,centerY;
  float radius;
  float opacity;
  
  Sun(){
    centerX = width/2;
    centerY = pondY;
    radius = height/2;
    
    y = 0;
    x = -radius;
    opacity=150;
  }
  void update(){
    if(me.ready){
      x = map(millis()-timer,0,60000,-radius,radius);
      y = -sqrt(sq(radius)-sq(x));
      opacity = map(millis()-timer,0,60000,150,255);
    }
    else {
      timer = millis();
    }
  }
  void draw(){
    pushMatrix();
    translate(centerX,centerY);
    fill(255,0,0,opacity);
    noStroke();
    ellipse(x,y,70,70);
    popMatrix();
  }
}
