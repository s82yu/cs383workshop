class Line{
  float x,y;
  float lineL,maxL;
  float foodX,foodY;
  boolean foodOnHook;
  
  Line(float _x,float _y){
    x = _x;
    y = _y;
    lineL=0;
    foodX = x;
    foodY = y+lineL;
    maxL = height/2;
    foodOnHook = true;
  }
  void update(float peakx,float peaky){
    x = peakx;
    y = peaky;
    
    if(me.ready){
      if(bkt.caughtFish.size()>=0){
        if(bkt.caughtFish.size()>=10 ){
          lineL -= moveY[0];
        }
        else {
          lineL += moveY[0];
        }
      }
      
      if(lineL <0){
        lineL=0;
      }
      if(lineL > maxL){
        lineL = maxL;
      }
    }
    foodX = x;
    foodY = y+lineL;
  }
  
  boolean foodInWater(){
    for(int i =0;i < agents.size();++i){
      if(agents.get(i).caught){
        //println("some one is still caught");
        foodOnHook = false;
        return false;
      }
    }
    if(foodY >= height-pondH && foodX > pondX){
      return true;
    }
    return false;
  }
  
  void draw(){
    pushMatrix();
    translate(x,y);
    stroke(255);
    strokeWeight(2);
    line(0,0,0,lineL);
    popMatrix();
  }
}
