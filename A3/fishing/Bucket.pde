class Bucket{
  float x1,y1;
  float x4,y4;
  float top=60,bot=42,h=80;
  ArrayList<Fish> caughtFish;
  float countX,countY;
  float waterX,waterY;
  float waterLevel;
  
  Bucket(float x,float y){
    x4 =x;
    y4= y;
    x1 = x4 - (top-bot)/2;
    y1 = y4 -h;
    caughtFish = new ArrayList<Fish>();
    countX = bot/2-5;//relative position x to x4
    countY = -h/2;//relative position y to y4
    waterLevel = 0.75;
    waterX = -waterLevel*(top-bot)/2;//relative position to x4
    waterY = -waterLevel*h;//relative tp y4
    
  }
  
  void addFish(Fish f){
    caughtFish.add(f);
  }
  
  void removeFish(Fish f){
    caughtFish.remove(f);
  }
  
  void update(){
    //println(waterLevel);
    
    waterLevel =0.5 + caughtFish.size()*0.05;
    if(waterLevel>1){
      waterLevel = 1;
    }
    
    waterX = -waterLevel*(top-bot)/2;//relative position to x4
    waterY = -waterLevel*h;//relative tp y4
  }
  void draw(){
    pushMatrix();
    //for(Fish f:caughtFish){
    //  f.update();
    //  f.draw();
    //}
    //bucket
    translate(x4,y4);
    
    stroke(0);
    strokeWeight(3);
    fill(255);
    quad(-(top-bot)/2,-h,-(top-bot)/2+top,-h,bot,0,0,0);
    noStroke();
    fill(0,0,220,200);
    quad(waterX,waterY,waterX+waterLevel*(top-bot)+bot,waterY,bot,0,0,0);
    //stroke(0);
    //strokeWeight(3);
    //arc(0.5*top,1/3*h,2/3*(top+bot),2/3*(top+bot),0,PI);
    //fill(0);
    //textSize(20);
    //text(bkt.caughtFish.size(),countX,countY);
    popMatrix();
    
  }
}
