
class Fish {

  //local shape transforms
  float shape;

  // location of agent centre and shape to display
  float x;//relative position x
  float y;//reletive pos y
  float X,Y;//global position x,y
  float fishDiameter;
  float speedx;
  float speedy;
  float G =5;
  boolean scared = false;
  boolean hungry = false;
  boolean caught = false;
  boolean inBkt = false;
  boolean jumping = false;
  boolean canStop = false;
  boolean inWater= true;
  boolean freed = false;
  boolean inBktArray,inOutArray,inPondArray;
  
  int r,g,b = 0;
  int opacity;
  
  //for triangle shape
  float triX1,triY1,triX2,triY2,triX3,triY3;

  // create the agent
  Fish(float _x, float _y) {
    x = _x;
    y = _y;
    X = x+pondX;
    Y = y+pondY;
    fishDiameter = random(15,35);
    shape = int(random(0,3.5));
    speedx = random(0,1);
    speedy = 0;
    opacity = int(random(200,255));
    r = int(random(0,255));
    g = int(random(0,255));
    b = int(random(0,255));
    inPondArray = true;
    inBktArray = false;
    inOutArray = false;
    
    //if is a triangle shape
    
    if(shape == 3){
      triX1 = random(-fishDiameter,-fishDiameter/3);
      triX2 = random(-fishDiameter/3,fishDiameter/3);
      triX3 = random(fishDiameter/3,fishDiameter);
      toss();
      if(coin <= 0.5){
        triY1 = sqrt(sq(fishDiameter)-sq(triX1));
      }
      if(coin > 0.5){
        triY1 = -sqrt(sq(fishDiameter)-sq(triX1));
      }
      toss();
      if(coin <= 0.5){
        triY2 = sqrt(sq(fishDiameter)-sq(triX2));
      }
      if(coin > 0.5){
        triY2 = -sqrt(sq(fishDiameter)-sq(triX2));
      }
      toss();
      if(coin <= 0.5){
        triY3 = sqrt(sq(fishDiameter)-sq(triX3));
      }
      if(coin > 0.5){
        triY3 = -sqrt(sq(fishDiameter/2)-sq(triX3));
      }
    }
  }
  
  void turn(){
    //turn back if hit wall
    if(inBkt){
      
      float minX,maxX,minY,maxY;
      
      minY = fishDiameter/2;
      maxY = bkt.waterLevel*bkt.h-fishDiameter/2;
      if(y >= maxY || y <= 0 && !jumping){
        speedy = -speedy;
        if(y >= maxY){
          y = maxY;
        }
        if(y <= minY){
          y=minY;
        }
      }
      maxX = bkt.top - y/bkt.h*(bkt.top-bkt.bot)/2 - fishDiameter/2;
      minX = bkt.top-maxX+fishDiameter/2;
      if(x >= maxX || x <= bkt.top-maxX){
        speedx = -speedx;
        if(x >= maxX){
          x = maxX;
        }
        if(x <=minX){
          x=minX;
        }
      }
    }
    else {
      if(x>=pondW-fishDiameter || x <= 0+fishDiameter){
        speedx=random(-1,1);
        speedx = -speedx;
        if(x >= pondW-fishDiameter){
          x = pondW-fishDiameter;
        }
        if(x <=fishDiameter){
          x=fishDiameter;
        }
      }
      if(y>=pondH - fishDiameter || y<=0 + fishDiameter){
        if(!scared){
          speedy=random(-1,1);
          speedy = -speedy;
        }
        if(y >= pondH - fishDiameter){
          y = pondH - fishDiameter;
        }
        if(y <=fishDiameter){
          y=fishDiameter;
        }
        //constrain(y,0,pondH);
      }
    }
  }
  
  void jump(){
    inWater();
    if(!inWater){
      if(inBkt){
        float maxX,minX;
        maxX = bkt.top - y/bkt.h*(bkt.top-bkt.bot)/2 - fishDiameter/2;
        minX = bkt.top-maxX+fishDiameter/2;
        if(x >= maxX || x <= bkt.top-maxX){
          speedx = -speedx;
          if(x >= maxX){
            x = maxX;
          }
          if(x <=minX){
            x=minX;
          }
        }
      }
      //println("im in the air",X,Y);
      //inBkt = false;
      canStop = true;
      speedy += 0.04;
      if(speedy >=2){
        speedy = 2;
      }
      if(X > landX && X < landX+landW && landY-Y < fishDiameter/2){
        speedx = 0;
        speedy = 0;
        freed = true;
        
        bkt.removeFish(this);
        inBktArray = false;
      }
      
      X += speedx;
      Y += speedy;
      
    }
    if(inWater && canStop) {
      //println("end");
      jumping = false;
      if(!inBkt){
        x = X-pondX;
        y = Y-pondY;
      } 
    }
    
  }
  
  boolean inWater(){
    float bktWaterX,bktWaterY,bktWaterMinX,bktWaterMaxX;
    bktWaterX = bkt.x4+bkt.waterX;
    bktWaterY = bkt.y4+bkt.waterY;
    bktWaterMaxX = bktWaterX+bkt.top - (Y-bkt.y1)/bkt.h*(bkt.top-bkt.bot)/2;
    bktWaterMinX = bktWaterX + (Y-bkt.y1)/bkt.h*(bkt.top-bkt.bot)/2;
    if(Y > bkt.y1 && Y < bkt.y1+bkt.h && X > bktWaterMinX && X < bktWaterMaxX){
      //println("in bkt water",bktWaterMinX,bktWaterMaxX);
      inBkt = true;
      inWater = false;
      if(Y > bktWaterY && Y <= bktWaterY+bkt.waterLevel*bkt.h){
        //println("in bkt water");
        inWater = true;
      }
      return inWater;
    }
    else if(X > pondX && X < pondX+pondW && Y > pondY && Y < pondY+pondH){
      inBkt = false;
      inWater = true;
      return inWater;
    }
    else if(caught){
      inWater = false;
      return inWater;
    }
    else {
      //println("out of water");
      inBkt = false;
      inWater = false;
      return inWater;
    }
  }
  
  //void alarm(){
  //  for(int i =0;i < agents.size();++i){
  //    agents.get(i).scared = true;
  //  }
  //}
  
  
  
  void update() {
    
    if(inWater){
      //check to see if hit the boundry
      turn();
      //normally update its mood to move around
      toss();
      if(coin < 0.5/frameRate){
        speedx = random(-1,1);
        toss();
        if(coin < 0.1){
          speedy = random(-1,1);
        }
        else {
          speedy =0;
        }
      } 
      
      //fish will be active if be scared
      if(abs(moveX[0]) > 70 || abs(moveY[0]) > 70){
        if(!inBkt){
          scared = true;
        } 
      }
      if(scared){
        speedy += random(0,0.3);
      }
      //calm stage
      if(abs(moveX[0]) < 40 || abs(moveY[0]) < 40){
        //if(timerStart()){
        //  timer = frameCount;
        //}
        if(frameCount % 180 ==0 && scared){
          scared = false;
          speedy = random(-0.2,1);
        }
        //check if the food is in the water;
        
        
        toss();
        if(coin < 0.1/frameRate){
          hungry = true;
        }
        //println(fishingLine.foodInWater(),fishingLine.foodOnHook,scared);
        //println(x+pondX,fishingLine.foodX);
        if(fishingLine.foodInWater()&&fishingLine.foodOnHook && !scared && hungry&&!inBkt){
          //println("fishing");
          speedy = random(-2,2);
          toss();
          if(abs(X-fishingLine.foodX) < 3 && abs(Y-fishingLine.foodY)<3 && coin < 0.2/chaos/frameRate){
            //println(x+pondX,fishingLine.foodX);
            caught =true;
          }
          
            if((Y-fishingLine.foodY)*speedy >= 0){
              speedy = -speedy;
            }
            if(X-fishingLine.foodX >= 5){
              speedx = (X-fishingLine.foodX)/(Y-fishingLine.foodY)*speedy;
            }
            if((X-fishingLine.foodX)*speedx >0){
              speedx = -speedx;
            }
        }
      }
      
    }
     //theres a chance for every fish in bkt to jump out of water
     if(!jumping && inBkt && inWater){
       toss();
       float jumpRate = map(chaos,0.8,3,1,10);
        if(coin < 0.02*jumpRate/frameRate){
          jumping = true;
          canStop = false;
          speedy = random(-2,-1);
        }
     }
    if(jumping){
      jump();
    }
    
    if(!scared){
      y += speedy*chaos*2;
      x += speedx*chaos*2;
    }
    if(scared){
      y += speedy*2;
      x += speedx*2;
    }
    

    if(caught){
      inWater = false;
      
      x = fishingLine.foodX;
      y = fishingLine.foodY;
      //if(x <= landX+landW && Y>=landY-fishDiameter){
      //  y = landY-fishDiameter;
      //}
      X = x;
      Y = y;
      if(x > bkt.x4+bkt.waterX && x < bkt.x1+bkt.top && y > bkt.y1){
        y = bkt.h+bkt.waterY+1;
        x = bkt.top/2;
        inBkt = true;
        caught = false;
        inWater = true;
        fishingLine.foodOnHook = true;
      }
    }
    
    //change to its global position
    if(inWater){
      if(!inBkt){
        X = x+pondX;
        Y = y+pondY;
      }
      else{
        X = x+bkt.x4+bkt.waterX;
        Y = y+bkt.y4+bkt.waterY;
      }
    }  
    if(!inWater){
      //println(x,y);
      
      constrain(X,0,width);
      constrain(Y,0,height);
      if(inBkt){
        x = X-(bkt.x4+bkt.waterX);
        y = Y-(bkt.y4+bkt.waterY);
      }
    }
  }

  void draw() {
    pushMatrix();    
    noStroke();
    fill(r,g,b,opacity);
    if(shape ==0){
      ellipse(X,Y,fishDiameter,fishDiameter);
    }
    if(shape == 1){
      rect(X-fishDiameter/2,Y-fishDiameter/2,fishDiameter,fishDiameter);
    }
    if(shape == 2){
      noFill();
      stroke(r,g,b,opacity);
      arc(X,Y,fishDiameter,fishDiameter,0,2*PI);
    }
    if(shape == 3){
      //translate(X,Y);
      triangle(X+triX1,Y+triY1,X+triX2,Y+triY2,X+triX3,Y+triY3);
    }
    
    popMatrix();
  }
}
