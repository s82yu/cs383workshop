class Rod{
  float x,y;
  float rodL;//length
  float peakX,peakY;
  float angleX,angleY;
  
  
  Rod(float _x,float _y){
    x=_x;
    y=_y;
    rodL =me.headsize*4;
    angleX=0;
    angleY=-rodL;
    peakX = x+angleX;
    peakY = y+angleY;
    
  }
  void update(float _x,float _y){
    x =_x;
    y =_y;
    if(me.bodyX == width-pondW-me.headsize/2){
      if(bkt.caughtFish.size()>=0){
        if(bkt.caughtFish.size()>=5){
          angleX -= moveX[0]/3;
        }
        else {
          angleX += moveX[0]/3;
        }
      }
      
      if(angleX >= rodL){
        angleX = rodL;
      }
      if(angleX <= -rodL){
        angleX = -rodL;
      }
      float tanY =sqrt(sq(rodL)-sq(angleX));
      angleY = -tanY;
    }
    peakX = x+angleX;
    peakY = y+angleY;
    
  }
  void draw(){
    pushMatrix();
    translate(x,y);
    stroke(0);
    strokeWeight(10);
    line(0,0,angleX,angleY);
    popMatrix();
  }
}
