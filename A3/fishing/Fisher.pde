class Fisher{
  float headX,headY;
  float headsize=40;
  boolean ready;
  float bodyX,bodyY,armX,armY,legX,legY,handX,handY,foot1X,foot1Y,foot2X,foot2Y;
  float bodyH,bodyW,legLen,armLen;
  float handSpeed,footSpeed;
  
  
  Fisher(){
    //legW = headsize*4.5/2;
    //legH = 10;
    legLen = headsize*4.5/2;
    armLen = headsize*3.5;
    bodyH = headsize*3.5;
    bodyW = 10;
    //wheelBackW = 8;
    //wheelBackH = bodyH*3/4;
    //wheelDiameter = wheelBackH;
    armX = 0;//relative position
    armY = -headsize;
    handX = armX;
    handY = armY + armLen;
    handSpeed = 1;
    footSpeed = 1;
    
    bodyX = headsize/2;//global centre positions
    bodyY = landY -legLen-bodyH/2;
    
    headX = 0;//relative pos
    headY = -bodyH/2-headsize/2+3;
    
    legX = 0;
    legY = bodyH/2;
 
    //wheelBackX = - 1.2*bodyW;
    //wheelBackY = bodyH/2-wheelBackH/2+5;
    
    //wheelX = wheelBackX/2;
    //wheelY = wheelBackH/2+headsize*1.5+10;
    //wheelx = wheelBackX+legW;
    //wheely = wheelBackY+wheelBackH/2+legW;
    
    ready = false;
    
  }
  void update(){
    
    //if you are not ready to fish yet, move around
    if(!ready){
      bodyX += moveX[0]; 
    }
    //dont cross the edge
    if(bodyX >= width-pondW-headsize/2){
      bodyX=width-pondW-headsize/2;
      if(fishingRod.angleX == fishingRod.rodL){
        ready = true;
      }
    }
    if(bodyX <= 0){
      bodyX =0;
    }
    
    
  }
  void draw(){
    pushMatrix();
    translate(bodyX,bodyY);
    noStroke();
    fill(0);
    ellipse(0,0,headsize,headsize);
    popMatrix();
  }
}
