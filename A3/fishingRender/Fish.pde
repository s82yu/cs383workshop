
class Fish {

  // local shape transforms
  //float scale = 1;
  float angle = 0;

  // location of agent centre and shape to display
  float x;
  float y;
  float fishDiameter =10;
  float speedx;
  float speedy;
  boolean scared = false;
  boolean hungry = false;
  boolean caught = false;
  boolean caughtInBkt = false;
  
  int r,g,b = 0;
  int opacity;

  float timer;

  // create the agent
  Fish(float _x, float _y) {
    x = _x;
    y = _y;
    speedx = random(0,1);
    speedy = 0;
    opacity = 255;
    r = int(random(0,255));
    g = int(random(0,255));
    b = int(random(0,255));
    timer= 0;
  }
  
  void turn(){
    //turn back if hit wall
    if(x>=pondW || x<= 0){
      speedx = -speedx;
      if(x>=pondW){
        x=pondW;
      }
      else {
        x=0;
      }
    }
    if(y>=pondH ||y<=0){
      if(!scared){
        speedy = -speedy;
      }
      if(y>=pondH){
        y=pondH;
      }
      else {
        y=0;
      }
    }
  }
  
  void alarm(){
    for(int i =0;i < agents.size();++i){
      agents.get(i).scared = true;
    }
  }
  
  boolean timerStart(){
    return timer - frameCount > 200;
  }
  
  void update() {
    //check to see if hit the boundry
    turn();
    //normally update its mood to move around
    toss();
    if(coin < 0.5/frameRate ){
      speedx = random(-1,1);
      toss();
      if(coin < 0.1){
        speedy = random(-1,1);
      }
      else {
        speedy =0;
      }
    } 
    
    //fish will be active if be scared
    if(abs(moveX[0]) > 50 || abs(moveY[0]) > 50){
        scared = true;
        
    }
    if(scared){
      speedy += random(0,0.3);
    }
    //calm stage
    if(abs(moveX[0]) < 40 || abs(moveY[0]) < 40){
      //if(timerStart()){
      //  timer = frameCount;
      //}
      if(frameCount % 250 ==0 && scared){
        scared = false;
        speedy = random(-0.2,1);
      }
      //check if the food is in the water;
      toss();
      if(coin < 0.8){
        hungry = true;
      }
      //println(fishingLine.foodInWater(),fishingLine.foodOnHook,scared);
      //println(x+pondX,fishingLine.foodX);
      if(fishingLine.foodInWater()&&fishingLine.foodOnHook&& hungry){
        //println("fishing");
        speedy = random(-2,2);
        toss();
        if(abs(x+pondX-fishingLine.foodX) < 3 && coin < 0.1/frameRate){
          caught =true;
          alarm();
        }
        if(!scared){
          if((y+pondY-fishingLine.foodY)*speedy >= 0){
            speedy = -speedy;
          }
          if(x+pondX-fishingLine.foodX >= 5){
            speedx = (x+pondX-fishingLine.foodX)/(y+pondY-fishingLine.foodY)*speedy;
          }
          if((x+pondX-fishingLine.foodX)*speedx >0){
            speedx = -speedx;
          }
        }
        
      }
    }
   
    y += speedy;
    x += speedx;

    if(caught){
      x = fishingLine.foodX;
      y = fishingLine.foodY;
      if(x > bkt.x1 && x < bkt.x1+bkt.top && y > bkt.y1){
        caughtInBkt = true;
        caught = false;
        fishingLine.foodOnHook = true;
      }
    }
  }

  void draw() {
    pg_render.beginDraw();
    if(!caught){
      pg_render.translate(width-pondW, height-pondH);
    }
    //rotate(radians(shapeAngle + angle));    
    pg_render.noStroke();
    pg_render.fill(r,g,b,opacity);
    if(!caughtInBkt){
      pg_render.rect(x,y,fishDiameter,fishDiameter);
    }
    
    pg_render.endDraw();
  }
}
