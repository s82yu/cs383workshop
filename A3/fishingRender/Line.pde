class Line{
  float x,y;
  float lineL,maxL;
  float foodX,foodY;
  boolean foodOnHook;
  
  Line(float _x,float _y){
    x = _x;
    y = _y;
    lineL=0;
    foodX = x;
    foodY = y+lineL;
    maxL = height*2/5;
    foodOnHook = true;
  }
  void update(float peakx,float peaky){
    x = peakx;
    y = peaky;
    
    if(me.ready){
      lineL += moveY[0]/2;
      if(lineL <0){
        lineL=0;
      }
      if(lineL > maxL){
        lineL = maxL;
      }
    }
    foodX = x;
    foodY = y+lineL;
  }
  
  boolean foodInWater(){
    for(int i =0;i < agents.size();++i){
      if(agents.get(i).caught){
        //println("some one is still caught");
        foodOnHook = false;
        return false;
      }
    }
    if(foodY >= height-pondH && foodX > pondX){
      return true;
    }
    return false;
  }
  
  void draw(){
    pg_render.beginDraw();
    pg_render.translate(x,y);
    pg_render.stroke(0);
    pg_render.strokeWeight(2);
    pg_render.line(0,0,0,lineL);
    pg_render.endDraw();
  }
}
