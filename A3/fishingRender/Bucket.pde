class Bucket{
  float x1,y1;
  float x4,y4;
  float top=60,bot=42,h=80;
  ArrayList<Fish> caughtFish;
  float countX,countY;
  
  Bucket(float x,float y){
    x4 =x;
    y4= y;
    x1 = x4 - (top-bot)/2;
    y1 = y4 -h;
    caughtFish = new ArrayList<Fish>();
    countX = top/2;//relative position x
    countY = h/2;//relative position y
  }
  void addFish(Fish f){
    caughtFish.add(f);
  }
  void update(){
    
  }
  void draw(){
    pg_render.beginDraw();
    for(Fish f:caughtFish){
      f.update();
      f.draw();
    }
    pg_render.translate(x1,y1);
    pg_render.stroke(0);
    pg_render.strokeWeight(5);
    pg_render.fill(255);
    pg_render.quad(0,0,top,0,top-(top-bot)/2,h,(top-bot)/2,h);
    pg_render.fill(255,0,0);
    pg_render.textSize(20);
    pg_render.text(bkt.caughtFish.size(),countX,countY);
    pg_render.endDraw();
  }
}
