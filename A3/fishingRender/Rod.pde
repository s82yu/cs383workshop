class Rod{
  float x,y;
  float rodL;//length
  float peakX,peakY;
  float angleX,angleY;
  
  
  Rod(float _x,float _y){
    x=_x;
    y=_y;
    rodL =200;
    angleX=0;
    angleY=-rodL;
    peakX = x+angleX;
    peakY = y+angleY;
    
  }
  void update(float _x,float _y){
    x =_x;
    y =_y;
    if(me.x == width-pondW-me.size/2){
      angleX += moveX[0]/5;
      if(angleX >= rodL){
        angleX = rodL;
      }
      if(angleX <= -rodL){
        angleX = -rodL;
      }
      float tanY =sqrt(sq(rodL)-sq(angleX));
      angleY = -tanY;
    }
    peakX = x+angleX;
    peakY = y+angleY;
    
  }
  void draw(){
    pg_render.beginDraw();
    pg_render.translate(x,y);
    pg_render.stroke(0);
    pg_render.strokeWeight(10);
    pg_render.line(0,0,angleX,angleY);
    pg_render.endDraw();
  }
}
