/*
 * Grid of shape agents that turn or scale relative to the mouse
 * - closely based on `P_2_1_1_04` from the book "Generative Design"
 *
 */


import de.looksgood.ani.*;
import processing.video.*;
//import processing.core.*;

import gab.opencv.*;
import org.opencv.imgproc.Imgproc;

//trees
import java.util.ArrayList;
import java.util.Random;

import com.thomasdiewald.pixelflow.java.DwPixelFlow;
import com.thomasdiewald.pixelflow.java.imageprocessing.filter.DwFilter;
import com.thomasdiewald.pixelflow.java.softbodydynamics.DwPhysics;
import com.thomasdiewald.pixelflow.java.softbodydynamics.constraint.DwSpringConstraint;
import com.thomasdiewald.pixelflow.java.softbodydynamics.constraint.DwSpringConstraint2D;
import com.thomasdiewald.pixelflow.java.softbodydynamics.particle.DwParticle;
import com.thomasdiewald.pixelflow.java.softbodydynamics.particle.DwParticle2D;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PShape;
import processing.opengl.PGraphics2D;

// tree objects
  Tree tree1;
  Tree tree2;
  Tree tree3;
  
  // parameters
  DwPhysics.Param          param_physics      = new DwPhysics.Param();
  DwParticle.Param         param_particles    = new DwParticle.Param();
  DwSpringConstraint.Param param_constraints  = new DwSpringConstraint.Param();
  
  // physics engine
  DwPhysics<DwParticle2D> physics;
 
  // list of all particles
  DwParticle2D[] particles;

  // render targets
  PGraphics2D pg_render;
  PGraphics2D pg_bloom;
  
  // pixelflow context
  DwPixelFlow context;
  
  boolean APPLY_WIND = true;
  //
  
Capture cam;
OpenCV opencv;
int cam_w = 640;
int cam_h = 480;

float scale = 0.25;
float coin;
PImage output;

// list of agents
ArrayList<Fish> agents;
Bucket bkt;
float bktBot = 42;
Fisher me;
Rod fishingRod;
Line fishingLine;
// number of fish
int fishNum; 
//fish pond
float pondW,pondH,pondX,pondY;
//land
float landW,landH,landX,landY;


int panelNum = 1;
PVector[] directions = new PVector[panelNum];
float[] baseY = new float[panelNum];
float[] moveY = new float[panelNum];
float[] baseX = new float[panelNum];
float[] moveX = new float[panelNum];


void setup() {
  
  fullScreen(P2D);
  
  pondW = width*3/5;
  pondH = height*2/5;
  pondX = width-pondW;
  pondY = height-pondH;
  landW = width-pondW;
  landH = height/2;
  landX = 0;
  landY = height/2;
  //number of fish
  fishNum = 100;
  
  cam_w = int(landW);
  cam_h = int(landH);
  cam = new Capture(this, int(cam_w * scale), int(cam_h * scale));
  opencv = new OpenCV(this, cam.width, cam.height);

  cam.start();
  output = new PImage(cam.width,cam.height);
 
  for(int i =0;i < panelNum;++i){
    directions[i] = new PVector();
    baseX[i]=0;
    baseY[i]=0;
    moveX[i]=0;
    moveY[i]=0;
  }
  
  // you have to call always Ani.init() first!
  Ani.init(this);

  createAgents(fishNum);
  
  // main library context
    context = new DwPixelFlow(this);
    context.print();
    context.printGL();
    


  //  // trees
  //  int tree_idx = 0;
  //  tree1 = new Tree(tree_idx++);
  //  tree1.param.LENGTH_LIMIT_MIN = 8;
  //  tree1.param.LENGHT_LIMIT_MAX = 120;
  //  tree1.param.LENGTH_MULT_BASE = 0.76f;
  //  tree1.style.BRANCH_STROKEWIDTH = 12;
  //  tree1.style.LEAF_RGBA = new float[]{255,128,0, 200};
  //  tree1.create(width/2, landY-1);

  //  tree2 = new Tree(tree_idx++);
  //  tree2.param.LENGTH_LIMIT_MIN = 10;
  //  tree2.param.LENGHT_LIMIT_MAX = 100;
  //  tree2.param.LENGTH_MULT_BASE = 0.73f;
  //  tree2.param.ANGLE_LIMIT_RANGE = PI/6;
  //  tree2.param.LEAF_CHANCE = 0.2f;
  //  tree2.style.BRANCH_STROKEWIDTH = 5;
  //  tree2.style.LEAF_RADIUS = 10;
  //  tree2.style.LEAF_RGBA = new float[]{32,160, 255, 200};
  //  tree2.create(100, landY-1);
    
  //  tree3 = new Tree(tree_idx++);
  //  tree3.param.LENGTH_LIMIT_MIN = 10;
  //  tree3.param.LENGHT_LIMIT_MAX = 100;
  //  tree3.param.LENGTH_MULT_BASE = 0.74f;
  //  tree3.param.LEAF_CHANCE = 1f;
  //  tree3.style.BRANCH_STROKEWIDTH = 5;
  //  tree3.style.LEAF_RADIUS = 10;
  //  tree3.style.LEAF_RGBA = new float[]{255,255,255, 255};
  //  tree3.create(width-300, landY-1);
    
    
  
  //  // particles
  //  int particles_offset = 0;
    
  //  param_particles = new DwParticle.Param();
  //  param_particles.DAMP_BOUNDS          = 0.50f;
  //  param_particles.DAMP_COLLISION       = 0.5990f;
  //  param_particles.DAMP_VELOCITY        = 0.920f; 
    
  //  particles = tree1.setParticles(particles, particles_offset, param_particles);
  //  particles_offset += tree1.node_count;
    
  //  param_particles = new DwParticle.Param();
  //  param_particles.DAMP_BOUNDS          = 0.50f;
  //  param_particles.DAMP_COLLISION       = 0.1990f;
  //  param_particles.DAMP_VELOCITY        = 0.980f; 

  //  particles = tree2.setParticles(particles, particles_offset, param_particles);
  //  particles_offset += tree2.node_count;
    
  //  param_particles   = new DwParticle.Param();
  //  param_particles.DAMP_BOUNDS          = 0.50f;
  //  param_particles.DAMP_COLLISION       = 0.1990f;
  //  param_particles.DAMP_VELOCITY        = 0.900f; 
    
  //  particles = tree3.setParticles(particles, particles_offset, param_particles);
  //  particles_offset += tree3.node_count;
    


  //  // physics
  //  param_physics.GRAVITY = new float[]{ 0, -0.000f };
  //  param_physics.bounds  = new float[]{ 0, 0, width, height };
  //  param_physics.iterations_collisions = 1;
  //  param_physics.iterations_springs    = 4;
    
  //  physics = new DwPhysics<DwParticle2D>(param_physics);
  //  physics.setParticles(particles, particles.length);
    

  //  // constraints
  //  param_constraints.damp_dec = 0.999999f;
  //  param_constraints.damp_inc = 0.999999f;
    
  //  tree1.setConstraints(physics, param_constraints);
  //  tree1.root.particle.enable(false, false, false);
    
  //  tree2.setConstraints(physics, param_constraints);
  //  tree2.root.particle.enable(false, false, false);
    
  //  tree3.setConstraints(physics, param_constraints);
  //  tree3.root.particle.enable(false, false, false);
    
    
    
    // render targets
    pg_bloom = (PGraphics2D) createGraphics(width, height, P2D);
    pg_bloom.smooth(8);
    
    pg_render = (PGraphics2D) createGraphics(width, height, P2D);
    pg_render.smooth(8);

    //pg_render.beginDraw();
    //pg_render.background(255);
    //pg_render.endDraw();
}

// create the grid of agents, one agent per grid location
void createAgents(int fishNum) {
  
  agents = new ArrayList<Fish>();
  me = new Fisher();
  fishingRod = new Rod(me.x,me.y);
  bkt = new Bucket(width*2/5-fishingRod.rodL/2,height/2);
  fishingLine = new Line(fishingRod.peakX,fishingRod.peakY);
  
  for(int i = 0;i<fishNum;++i){
    float x=random(0,pondW);
    float y=random(0,pondH);
    Fish a = new Fish(x, y);
    agents.add(a);
  }
  
}

  void toss(){
    coin = random(0,1);
  }
  
void draw() {
  background(255);
  
  pushMatrix();
  //pond draw
  
  popMatrix();
  
  //pushMatrix();
  //translate(landX,landY);
  //scale(1/scale*0.95);
  //tint(255);
  //image(output,0,0);
  //popMatrix(); 
  
  if (cam.available() == true) {
    cam.read();

    // load frame into pipeline 
    opencv.loadImage(cam);

    // mirror
    opencv.flip(1);

    opencv.calculateOpticalFlow();
    // calculate average direction
    directions[0] = opencv.getAverageFlowInRegion(0,0,cam.width,cam.height);
    //directions[1] = opencv.getAverageFlowInRegion(0,0,cam.width/2,cam.height);
    //directions[2] = opencv.getAverageFlowInRegion(cam.width/2,0,cam.width/2,cam.height);

    // if motion is very small, optical flow might return NaN
    for(int i = 0;i < panelNum;++i){
       if (Float.isNaN(directions[i].x) || Float.isNaN(directions[i].y)) {
          directions[i] = new PVector();
       }
    }
    
  }
  
  for(int i = 0;i < panelNum;i++){
    PVector a = new PVector();
    if(i == 0){
      a = new PVector(width/2, height/2);
    }
    if(i == 1){
      a = new PVector(width/4, height/2);
    }
    if(i == 2){
      a = new PVector(width/4*3, height/2);
    }
    
 
    PVector b = PVector.add(a, PVector.mult(directions[i], 100));
    moveX[i] = b.x-a.x;
    moveY[i] = b.y-a.y;
    if (Float.isNaN(moveX[i]) || Float.isNaN(moveY[i])) {
       println(width,height,a,directions[i]);
    }
    if(abs(moveX[i])<5){
      moveX[i] =0;
    }
    if(abs(moveY[i])<5){
      moveY[i]=0;
    }
    baseX[i] += moveX[i];
    baseY[i] += moveY[i];
    constrain(baseX[i],0,width);
    constrain(baseY[i],0,height);
  }
  
    me.update();
    bkt.update();
    fishingRod.update(me.x,me.y);
    fishingLine.update(fishingRod.peakX,fishingRod.peakY);
    
    
    for(int i = 0;i < agents.size();++i){
      if(agents.get(i).caughtInBkt){
        bkt.addFish(agents.get(i));
        agents.remove(agents.get(i));
        //println(agents.size());
      }
    }
    
    
    
    //// add additional forces, e.g. Wind, ...
    //int particles_count = physics.getParticlesCount();
    //DwParticle[] particles = physics.getParticles();
    //if(APPLY_WIND){
    //  float[] wind = new float[2];
      
    //  float speed_x = 1;
    //  float speed_y = 1;
      
    //  float wind_x = -sin(PI/2 + frameCount*0.01f * speed_x) * 0.05f * speed_x;
    //  float wind_y = -sin(PI/2 + frameCount*0.02f * speed_y) * 0.05f * speed_y;
      
    //  wind[0] = wind_x;
    //  wind[1] = wind_y;
      
    //  for(int i = 0; i < particles_count; i++){
    //    particles[i].addForce(wind);
    //  }
    //}

    
    //physics.update(1);
    

    //tree1.createShape(this);
    //tree2.createShape(this);
    //tree3.createShape(this);
    
    pg_render.beginDraw();
    pg_render.noStroke();
    pg_render.fill(200,96);
    pg_render.rect(0, 0, width, height);

    pg_render.background(0);
    
    pg_render.fill(0,0,100,150);
    pg_render.rect(pondX,pondY,pondW,pondH);
    //land draw
    pg_render.fill(0);
    pg_render.rect(landX,landY,landW,landH);
    
    bkt.draw();
    for (Fish ag : agents) {
      ag.update();
      ag.draw();
    }
   
    me.draw();
    fishingRod.draw();
    fishingLine.draw();
   
    //tree1.display(pg_render);
    //tree2.display(pg_render);
    //tree3.display(pg_render);
    
    //pg_render.shape(tree1.shp_tree);
    //pg_render.shape(tree2.shp_tree);
    //pg_render.shape(tree3.shp_tree);
    
    pg_render.endDraw();


    DwFilter filter = DwFilter.get(context);
    filter.bloom.param.mult   = 1.0f;
    filter.bloom.param.radius = 1;
    filter.bloom.apply(pg_render, pg_bloom, pg_render);  

    blendMode(REPLACE);
    background(0);
    image(pg_render, 0, 0);
    
    fill(255, 0, 0);
    text(nfc(frameRate, 1), 20, 20);
}
