static class Tree{
    
    public static class Param{
      public Random rand_tree_angle  = new Random(1);
      public Random rand_tree_length = new Random(10);
      public Random rand_leaf        = new Random(0);
      
      public float ANGLE_LIMIT_RANGE = PI/5.0f;
      public int   DEPTH_LIMIT_MAX   = 15;
      public int   LENGTH_LIMIT_MIN  = 8;
      public int   LENGHT_LIMIT_MAX  = 120;
      
      public float LENGTH_MULT_BASE  = 0.76f;
      public float LENGTH_MULT_RANGE = 0.22f;
      
      public float LEAF_CHANCE  = 0.1f;
    }
    
    public static class Style{
      public float   BRANCH_STROKEWIDTH = 15;
    
      public float   LEAF_RADIUS  = 20;
      public float[] LEAF_RGBA  = {255, 128, 0, 150};
    }
    
    
    public int idx = 0;
    public int MAX_DEPTH = 0;
    public float MAX_LENGTH = 0;
    public int node_count = 0;
    
    public Param param = new Param();
    public Style style = new Style();
    
    public Node root;
    
    public PShape shp_tree;
 
    public Tree(int idx){
      this.idx = idx;
    }
    
    public void create(float px, float py){
      param.rand_tree_angle = new Random(0);
      param.rand_tree_length = new Random(0);
      node_count = 0;
      root = new Node(this, px, py, -PI/2, param.LENGHT_LIMIT_MAX);
    }
    
    public void display(PGraphics2D canvas){
      param.rand_leaf = new Random(0);
      root.display(canvas);
    }
    
    public int nodeCount(){
      return node_count;
    }
    
    public DwParticle2D[] setParticles(DwParticle2D[] particles, int offset, DwParticle.Param param){
      // dynamically resize array if required
      if(particles == null || particles.length < offset + node_count){
        DwParticle2D[] particles_tmp = new DwParticle2D[offset + node_count];
        if(particles != null){
          System.arraycopy(particles, 0, particles_tmp, 0, offset);
        }
        particles = particles_tmp;
      }
      if(root != null) root.setParticles(particles, offset, param);
      return particles;
    }
    
    public void setConstraints(DwPhysics<DwParticle2D> physics, DwSpringConstraint.Param param){
      if(root != null) root.setConstraints(physics, param);
    }
    
    
    public void createShape(PApplet papplet){
      param.rand_leaf = new Random(0);
      if(shp_tree == null){
        shp_tree = papplet.createShape(PConstants.GROUP);
      }
      
      root.createShape(papplet, shp_tree);
    }
    
  }
