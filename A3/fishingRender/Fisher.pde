class Fisher{
  float x,y;
  float size=50;
  boolean ready;
  
  
  Fisher(){
    x = size/2;
    y = height/3;
    ready = false;
    
  }
  void update(){
    
    //if you are not ready to fish yet, move around
    if(!ready){
      x += moveX[0];
    }
    //dont cross the edge
    if(x >= width-pondW-size/2){
      x=width-pondW-size/2;
      if(fishingRod.angleX == fishingRod.rodL){
        ready = true;
      }
    }
    if(x <= 0){
      x =0;
    }
    
    
  }
  void draw(){
    pg_render.beginDraw();
    pg_render.translate(x,y);
    pg_render.fill(0);
    pg_render.ellipse(0,0,size,size);
    pg_render.endDraw();
  }
}
