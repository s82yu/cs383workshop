static class Node{

    final Tree tree;
    
    Node parent = null;
    
    Node child1 = null;
    Node child2 = null;
    
    boolean is_leaf = false;
    boolean is_root = false;

    int idx = 0;
    int depth = 0;
    float angle, length;
    DwParticle2D particle;

    public void initParticle(float px, float py){
      float radius = length/2;
      float radius_collision = Math.max(length/3, 2);
//      radius_collision = Math.min(radius_collision, 10);
//      radius_collision = 1;
      
      particle = new DwParticle2D(idx);
      particle.setPosition(px, py);
      particle.setMass(1);
      particle.setRadius(radius);
      particle.setRadiusCollision(radius_collision);
      particle.setCollisionGroup(tree.idx); 
    }
    
    
    public Node(Tree tree, float px, float py, float angle, float length){
      this.tree = tree;
      this.idx = tree.node_count++;
      this.angle = angle;
      this.length = length;
      this.is_leaf = length < tree.param.LENGTH_LIMIT_MIN || depth > tree.param.DEPTH_LIMIT_MAX;
      this.is_root = parent == null;

      initParticle(px, py);
      
      child1 = new Node(tree, this, 0);
    }
    

    public Node(Tree tree, Node parent, float angle_scale){
      this.tree = tree;
  
      this.parent = parent;
      this.depth = parent.depth + 1;
      this.idx = tree.node_count++;
      
      float rand_angle  = tree.param.rand_tree_angle.nextFloat();
      float rand_length = tree.param.rand_tree_length.nextFloat();
       
      float length_mult = tree.param.LENGTH_MULT_BASE + (rand_length * 2 - 1) * tree.param.LENGTH_MULT_RANGE;
      length_mult = Math.min(Math.max(length_mult, 0), 1);
      
      this.length = parent.length * length_mult;
      this.angle  = parent.angle + rand_angle * angle_scale;
      
      this.is_leaf = length < tree.param.LENGTH_LIMIT_MIN || depth > tree.param.DEPTH_LIMIT_MAX;
      this.is_root = parent == null;
      
      float px = parent.particle.cx + cos(angle) * length;
      float py = parent.particle.cy + sin(angle) * length;
      
      initParticle(px, py);
      
      tree.MAX_DEPTH  = Math.max(tree.MAX_DEPTH , depth);
      tree.MAX_LENGTH = Math.max(tree.MAX_LENGTH, length);
      
      if(!is_leaf){
        child1 = new Node(tree, this, -tree.param.ANGLE_LIMIT_RANGE);
        child2 = new Node(tree, this, +tree.param.ANGLE_LIMIT_RANGE);
      }
    }
    
    public int nodeCount(int count){
      if(child1 != null) count = child1.nodeCount(count);
      if(child2 != null) count = child2.nodeCount(count);
      return count + 1;
    }
    
    public void setParticles(DwParticle2D[] particles, int offset, DwParticle.Param param){
      particle.setParamByRef(param);
      particles[offset + idx] = particle;
      if(child1 != null) child1.setParticles(particles, offset, param);
      if(child2 != null) child2.setParticles(particles, offset, param);
    }
    
    public void setConstraints(DwPhysics<DwParticle2D> physics, DwSpringConstraint.Param param){
      Node othr = parent;
      int counter_max = 2;
      int counter = 0;
      while(othr != null && counter < counter_max){
        DwSpringConstraint2D.addSpring(physics, othr.particle, this.particle, param);
        othr = othr.parent;
        counter++;
      }
      
      
      if(parent != null){
        DwSpringConstraint2D.addSpring(physics, parent.particle, this.particle, param);
        Node sibling = parent.child1 != this ? parent.child1 : parent.child2;
        if(sibling != null){
          DwSpringConstraint2D.addSpring(physics, sibling.particle, this.particle, param);
        }
      }
      
      
      if(child1 != null) child1.setConstraints(physics, param);
      if(child2 != null) child2.setConstraints(physics, param);
    }
    
    PShape shp_branch;
    PShape shp_leaf;
    
    public void createShape(PApplet papplet, PShape shp_tree){

      float depthn  = (this.depth +1) / (float) (tree.MAX_DEPTH +1);
      float lengthn = (this.length+1) / (float) (tree.MAX_LENGTH+1);
      float parent_lengthn = lengthn;
      if(parent != null){
        parent_lengthn = (parent.length+1) / (float) (tree.MAX_LENGTH+1);
      }
      
      if(is_leaf){
        float chance = tree.param.rand_leaf.nextFloat();
        if(chance < tree.param.LEAF_CHANCE){
          float radius = Math.max(depthn * tree.style.LEAF_RADIUS, 1);
          DwParticle2D pb = parent.particle;  
          if(shp_leaf == null){
            
            int r = (int) (tree.style.LEAF_RGBA[0] * depthn);
            int g = (int) (tree.style.LEAF_RGBA[1] * depthn);
            int b = (int) (tree.style.LEAF_RGBA[2] * depthn);
            int a = (int) (tree.style.LEAF_RGBA[3]         );
            int argb = a << 24 | r << 16 | g << 8 | b;
            
            shp_leaf = papplet.createShape(PConstants.ELLIPSE, 0, 0, radius, radius);
            shp_leaf.setStroke(false);
            shp_leaf.setFill(true);
            shp_leaf.setFill(argb);
               shp_tree.addChild(shp_leaf);
          }
          shp_leaf.resetMatrix();
          shp_leaf.translate(pb.cx, pb.cy);
        }
      } else {
        
        float sw_pa = Math.max(lengthn        * tree.style.BRANCH_STROKEWIDTH, 1);
        float sw_pb = Math.max(parent_lengthn * tree.style.BRANCH_STROKEWIDTH, 1);
        
        if(!is_root){
          DwParticle2D pa = particle;
          DwParticle2D pb = parent.particle;
          
          if(shp_branch == null){
            shp_branch = papplet.createShape();
            shp_branch.beginShape(PConstants.LINES);
            shp_branch.strokeCap(PConstants.ROUND);

            shp_branch.strokeWeight(sw_pb);
//            shp_branch.vertex(pa.cx, pa.cy);
//            shp_branch.vertex(pb.cx, pb.cy);
            shp_branch.stroke(parent_lengthn * 128);
            shp_branch.vertex(0, 0);
            shp_branch.stroke(lengthn * 128);
            shp_branch.vertex(length, 0);
            shp_branch.endShape();
            shp_tree.addChild(shp_branch);
          }
          
          float dx = pa.cx - pb.cx;
          float dy = pa.cy - pb.cy;
          float dd = (float) Math.sqrt(dx*dx + dy*dy);
          float angle = PApplet.atan2(dy, dx);
          
          shp_branch.resetMatrix();
          shp_branch.scale(dd/length);
          shp_branch.rotate(angle);
          shp_branch.translate(pb.cx, pb.cy);
        }
        
      }
      
      if(child1 != null) child1.createShape(papplet, shp_tree);
      if(child2 != null) child2.createShape(papplet, shp_tree);
    }
      
    
    
    
    public void display(PGraphics2D canvas){
      float depthn  = (this.depth +1) / (float) (tree.MAX_DEPTH +1);
      float lengthn = (this.length+1) / (float) (tree.MAX_LENGTH+1);
      float parent_lengthn = lengthn;
      if(parent != null){
        parent_lengthn = (parent.length+1) / (float) (tree.MAX_LENGTH+1);
      }
      
      
      if(is_leaf){
        float chance = tree.param.rand_leaf.nextFloat();
        if(chance < tree.param.LEAF_CHANCE){
          float radius = Math.max(depthn * tree.style.LEAF_RADIUS, 1);
          DwParticle2D pb = parent.particle;  
          canvas.fill(tree.style.LEAF_RGBA[0] * depthn, tree.style.LEAF_RGBA[1] * depthn, tree.style.LEAF_RGBA[2] * depthn, tree.style.LEAF_RGBA[3]);
          canvas.noStroke();
          canvas.ellipse(pb.cx, pb.cy, radius, radius);
        }
      } else {
        
        float sw_pa = Math.max(lengthn        * tree.style.BRANCH_STROKEWIDTH, 1);
        float sw_pb = Math.max(parent_lengthn * tree.style.BRANCH_STROKEWIDTH, 1);
        
        if(!is_root){
          DwParticle2D pa = particle;
          DwParticle2D pb = parent.particle;
          canvas.noFill();
        
//          canvas.strokeWeight(sw_pa);
//          canvas.stroke(lengthn * 128);
//          canvas.line(pa.cx, pa.cy, pb.cx, pb.cy);
//          
          
          canvas.beginShape(PConstants.LINES);
          
          canvas.stroke(lengthn * 128);
          canvas.strokeWeight(sw_pa);
          canvas.vertex(pa.cx, pa.cy);
          
//          canvas.stroke(parent_lengthn * 128);
//          canvas.strokeWeight(sw_pb);
          canvas.vertex(pb.cx, pb.cy);
          
          canvas.endShape();
        }
        
      }
      
      if(child1 != null) child1.display(canvas);
      if(child2 != null) child2.display(canvas);
    }
  }
