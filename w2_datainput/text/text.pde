/*
 * text data demo
 *   Loads text and prints it to the console. 
 */

String all;
float prevX;

void setup() {
  
  size(600,800);
  background(0);
 
  // you can also load from the web by giving a URL
  //String fn = "http://www.gutenberg.org/cache/epub/2489/pg2489.txt";
  String fn = "https://raw.githubusercontent.com/processing/processing/master/core/src/processing/core/PApplet.java";
  
  //String fn = "1342-0.txt";
  println("Loading `" + fn + "'...");
  String[] lines = loadStrings(fn);
  println("  loaded " + lines.length + " lines");  
  
  // join the lines back into one big string (with new lines inserted)
  all = String.join("\n", lines);
 
  textSize(25);
    for(int i = 0;i < all.length();i++){
      char c = all.charAt(i);
      float x = random(0,600);
      float y = random(0,800);
      if(c == ')' || c ==' '){
        fill(255);
        text(c,x,y);
      }
    }
  
  //println(all);
}

void draw(){
  if(prevX != mouseX){
    background(0);
    
    for(int i = 0;i < all.length();i++){
      char c = all.charAt(i);
      float x = random(0,600);
      float y = random(0,800);
      if(c == ')'){
        fill(255);
        float fontsize =random(10,30);
        textSize(fontsize);
        text(c,x,y);
      }
    }
    prevX = mouseX;
  }
  
}