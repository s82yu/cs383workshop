import processing.video.*;

import gab.opencv.*;
// also access "native" OpenCV calls
import org.opencv.imgproc.Imgproc;

Capture cam;
OpenCV opencv;

// scale factor to downsample frame for processing 
float scale = 0.5;

// image to display
PImage output;

// dominant direction of image calculated by optical flow
//PVector direction;
PVector[] directions;

int seq=0;

PImage pic1;
PImage[] pics;
PImage[] savedpics;
PImage temp;
int picNum = 3;
int curNum = 0;
Boolean savepic = false;
int saveCount = 0;
int stage=1;
int maxstage = 7;
boolean next;
boolean end = false;

boolean fullScreen;
float fullScreenScale;

int panelNum = 3;
float[] baseY = new float[panelNum];
float[] mapY = new float[panelNum];
float[] moveY = new float[panelNum];
float[] baseX = new float[panelNum];
float[] mapX = new float[panelNum];
float[] moveX = new float[panelNum];
float imgscale;



void setup() {
  //size(640, 480);
  
  fullScreen(); 
  //fullScreen = true;
  //fullScreenScale = width / float(640);

  // want video frame and opencv proccesing to be same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));
  opencv = new OpenCV(this, cam.width, cam.height);
  
  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height);

  directions = new PVector[panelNum];
  for(int i =0;i < panelNum;i++){
    directions[i] = new PVector(0,0);
  }
  
  pic1 = loadImage("1.jpg");
  pics = loadImages("./data/",picNum);
}

void draw() {
  // clear background to black
  background(0);
  //pushMatrix();
  //if (fullScreen) { 
  //  scale(fullScreenScale, fullScreenScale);
  //}

  if (cam.available() == true) {
    cam.read();
    
    // load frame into pipeline 
    //seq as sequence
    //for(int seq = 0; seq < 2;seq++){
        opencv.loadImage(cam); 
          
      // mirror
      opencv.flip(1);
    
      opencv.calculateOpticalFlow();
    
      // calculate average direction
      if(seq == 0){
        directions[seq] = opencv.getAverageFlowInRegion(0,0,160,240);
      }
      if(seq == 1){
        directions[seq] = opencv.getAverageFlowInRegion(160,0,160,240);
      }
      if(seq == 2){
        directions[seq] = opencv.getAverageFlowInRegion(0,0,320,240);
      }
      //if(seq == 3){
      //  directions[seq] = opencv.getAverageFlowInRegion(0,0,320,240);
      //}
      
    
      // if motion is very small, optical flow might return NaN
      if (Float.isNaN(directions[seq].x) || Float.isNaN(directions[seq].y)) {
        directions[seq] = new PVector();
      }
    //}
    output = opencv.getSnapshot();
  }
  
  //pushMatrix();
  //scale(1 / scale);
  ////image(output,0,0);
  
  ////draw optical flow vector field for debug
  //stroke(255, 128);
  //strokeWeight(1);
  //opencv.drawOpticalFlow();
  //popMatrix();
  
  
  for(int i = 0;i < panelNum;i++){
    PVector a = new PVector();
    if(i == 0){
      a = new PVector(width/4, height/2);
    }
    if(i == 1){
      a = new PVector(width/4*3, height/2);
    }
    if(i == 2){
      a = new PVector(width/2, height/2);
    }
    //if(i == 3){
    //  a = new PVector(width/4*3, height/4*3);
    //}
    
    PVector b = PVector.add(a, PVector.mult(directions[i], 25));
    
    moveX[i] = b.x - a.x;
    //ignore the tiny motions
    if(abs(moveX[i]) < 2){
      moveX[i] = 0;
    }

    
    baseX[i] = baseX[i] + moveX[i];
    if(baseX[i] > 200){
      baseX[i] = 200;
    }
    if(baseX[i] < -200){
      baseX[i] = -200;
    }
    mapX[i] = map(baseX[i],-200,200,0,255);
    //variable for stage2
    imgscale = map(baseX[1]- baseX[0],-400,400,0.2,5);
    
    moveY[i] = (b.y - a.y);
    //ignore the tiny motions
    if(abs(moveY[i]) < 2){
      moveY[i] =0;
    }
    
    baseY[i] = baseY[i] + moveY[i];
    if(baseY[i] > 200){
      baseY[i] = 200;
    }
    if(baseY[i] < -200){
      baseY[i] = -200;
    }
    mapY[i] = map(baseY[i],-200,200,255,0);
    
    //println(baseY[i]);
    
    
    //draw direction vector
    //stroke(255, 255, 0, 128);
    //strokeWeight(4);
    //line(a.x, a.y, b.x, b.y);
  }
      
  //tint
  if(stage == 1){
    next = false;
    if(curNum == picNum){
      end = true;
    }
    
    if(!end){
      tint(255);
      image(pics[curNum],0,0,width,height);
      //popMatrix();
      
    }
    else {
      fill(255,255,255);
      textSize(50);
      textAlign(CENTER);
      text("End",640,480);
    }
  }
  if(stage == 2){
    if(curNum == 0){
      fill(255,255,255);
      textSize(50);
      textAlign(CENTER);
      text("RGB Value",320,160);
      textSize(35);
      textAlign(LEFT);
      text("Instructions: horizontal movement changes green value",320,300);
      text("vertical movement changes red and blue value",320,350);
    }
    else {
      stage++;
    }
  }
  if(stage == 3){
    pushMatrix();
    tint(mapY[2],mapX[2],255-mapY[2]);
    image(pics[curNum],0,0,width,height);
    popMatrix();
    
    
  }
  if(stage == 4){
    if(curNum == 0){
      fill(255,255,255);
      textSize(50);
      textAlign(CENTER);
      text("Resize",320,160);
      textSize(35);
      textAlign(LEFT);
      text("Instructions: pushing will shrink the image",320,300);
      text("pulling out will zoom in the image",320,350);
    }
    else {
      stage++;
    }
  }
  //scale
  if(stage == 5){
    pushMatrix();
    //translate(width/2, height/2);
    float tranX = map(baseX[2],-200,200,0,width);
    float tranY = map(baseY[2],-200,200,0,height);
    translate(tranX, tranY);
    imageMode(CENTER);
    scale(1/imgscale);
    image(pics[curNum],0,0,width,height);
    popMatrix();
    temp = get();
  }
  if(stage == 6){
    fill(255,255,255);
    textSize(50);
    textAlign(CENTER);
    text("Preview",640,480);
  }
  if(stage == 7){
    pushMatrix();
    translate(0, 0);
    imageMode(CORNER);
    tint(255);
    image(temp,0,0,width,height);
    popMatrix();
    
    if (savepic) {

      // save the frame (nf just adds leading zeros)
      save("./data/" + nf(saveCount, 1) + "_copy" + ".jpg");
      println("picture saved!");

      // increment save frame count
      saveCount++;

      // reset the flag
      savepic = false;
      
      fill(255,255,255);
      textSize(50);
      textAlign(CENTER);
      text("Image saved",640,480);
    }
    
    if(next){
        //println(curNum);
        curNum++;
        stage=1;
      }
  }
  // debug output
  //fill(255, 0, 0);
  
  //text(nfc(frameRate, 1), 20, 20);
  
  
  if(seq == panelNum-1){
    seq = 0;
  }
  else{
    seq++;
  }
  //popMatrix();
}

PImage[] loadImages(String path,int n){
  PImage[] Images = new PImage[n];
  for (int i = 0;i < n;i++){
    PImage thispic = loadImage(path + nf(i,1) + ".jpg");
    Images[i] = thispic;
  }
  return Images;
}

void keyPressed(){
  if(key == ' ' && stage <= maxstage){
    if(stage == maxstage){
      savepic = true;
      next = true;
    }
    else if(!end) {
      //println(stage);
      stage++;
    }
  }
}