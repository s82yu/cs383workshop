import websockets.*;

// needed for reliabile transfering data from message thread to drawing thread
import java.util.concurrent.ConcurrentLinkedQueue;

WebsocketServer ws;

void setup() {
  size(400, 400);

  // start server
  ws = new WebsocketServer(this, 3001, "");

  background(0);
}

void draw() {

  // process the queue of messages
  while (!q.isEmpty()) {
    // pull off message from tail and paint it as ellipse
    Message m = q.poll();
    noStroke();
    fill(255);
    ellipse(m.x, m.y, 10, 10);
  }
}

void keyPressed() {
  int r = int(random(0,255));
  int g = int(random(0,255));
  int b = int(random(0,255));
  ws.sendMessage(r + "," +g + "," + b);
}

// record to hold each incoming message
class Message {

  Message(String msg) {
    String[] t = msg.split(",");
    x = int(t[0]);
    y = int(t[1]);
  }

  String toString() {
    return x + "," + y;
  }

  int x;
  int y;
}

// special queue that works across threads
ConcurrentLinkedQueue<Message> q = new ConcurrentLinkedQueue<Message>();

// callback when client sends message
// NOTE: this isn't on the drawing thread, so you can't draw here
void webSocketServerEvent(String msg) {


  Message m = new Message(msg);
  println(m);
  Message k = new Message(msg);
  println(k);
  
  // drawing here may not work because of thread boundaries
  //noStroke();
  //fill(255);
  //ellipse(m.x, m.y, 10, 10);

  // add message to the queue
  q.offer(m);
  q.offer(k);
}