import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Map;
import java.util.Set;


class InputState {

  void update(int f, int x, int y, boolean pressed) {
    if (f != frameNum) {
      pmouseX = mouseX;
      pmouseY = mouseY;
    }
    mouseX = x;
    mouseY = y;
    mousePressed = pressed;
    // to check for dead clients
    frameNum = f;
  }

  String toString() {
    return frameNum + ": " + mousePressed + 
      ",(" + pmouseX + "," + pmouseY + ")," +
      "(" + mouseX + "," + mouseY + ")";
  }

  int frameNum;
  int mouseX;
  int mouseY;
  int pmouseX;
  int pmouseY;
  boolean mousePressed;
}

HashMap<String, InputState> clients = new HashMap<String, InputState>();

ConcurrentLinkedQueue<Message> q = new ConcurrentLinkedQueue<Message>();

class Message {

  Message(String msg) {
    String[] t = msg.split(",");
    id = t[0];
    e = t[1];
    x = int(t[2]);
    y = int(t[3]);
  }

  String toString() {
    return id + "," + e + "," + x + "," + y;
  }

  String id;
  String e;
  int x;
  int y;
}


void updateClientStates() {

  // update state of all clients
  while (!q.isEmpty()) {
    Message m = q.poll();

    if (!clients.containsKey(m.id)) {
      println("New Client: " + m.id);
      clients.put(m.id, new InputState());
    }

    if (m.e.equals("mm") || m.e.equals("md")) {
      clients.get(m.id).update(frameCount, m.x, m.y, m.e.equals("md"));
    }
  }
}


void webSocketServerEvent(String msg) {
  Message m = new Message(msg);
  //println(m);
  q.offer(m);
}