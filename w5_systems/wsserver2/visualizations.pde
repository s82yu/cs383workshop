
abstract class Viz {
  abstract void draw();
}

// each client draws a line
class DrawingViz extends Viz {

  void draw() {

    for (String c : clients.keySet()) {

      InputState i = clients.get(c);

      if (i.mousePressed) {
        d.beginDraw();
        d.stroke(255);
        d.strokeWeight(10);
        d.line(i.mouseX, i.mouseY, i.pmouseX, i.pmouseY);
        d.endDraw();
      }
    }
  }
}


// some clients erase, some draw
// each client draws a line
class CompetitiveDrawingViz extends Viz {

  int lastSwitchTime = 0;
  String erasingClient = "";

  void draw() {

    int pick = -1;
    if (clients.size() > 1) {
      if (erasingClient.equals("") || lastSwitchTime + 5000 < millis()) {
        Set<String> keys = clients.keySet();
        String[] a = keys.toArray(new String[keys.size()]);
        erasingClient = a[int(random(a.length))];
        println(erasingClient + " will erase");
        lastSwitchTime = millis();
      }
    }


    for (String c : clients.keySet()) {

      InputState i = clients.get(c);

      if (i.mousePressed) {
        d.beginDraw();
        d.noStroke();
        if (c == erasingClient) {
          d.fill(0);
          d.ellipse(i.mouseX, i.mouseY, 20, 20);
        } else {
          d.fill(255);
          d.ellipse(i.mouseX, i.mouseY, 10, 10);
        }
        d.endDraw();
      }
    }
  }
}


// joins all clients into a polygon
class PolygonViz extends Viz {

  void draw() {

    d.beginDraw();
    d.background(0, 255);
    d.stroke(255, 255);
    d.noFill();
    d.beginShape();
    for (String c : clients.keySet()) {

      InputState i = clients.get(c);

      d.vertex(i.mouseX, i.mouseY);
    }

    d.endShape(CLOSE);
    d.endDraw();
  }
}