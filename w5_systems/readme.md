# Workshop 5: Systems Design

We'll explore the idea of connecting multiple sketches together and create a platform for mouse-based multi-person performance art.


## Goals

* Learn about client-server networking with simple websockets
* Brainstorm related art works


# Pre-workshop Set Up

Complete the following _before_ the Workshop 5 class.

#### 1. Install required libraries

* [**Websockets**](https://github.com/alexandrainst/processing_websockets), a very simple websocket implementation for Processing )


> Use the menu `Sketch/Import Library.../Add Library...`, in the dialogue that opens, search for the library name and click "Install". 


> Post to slack if you have trouble with set up. Please provide details so we can diagnose (e.g. operating system, error messages, steps to reproduce the error) 


# In-Class Workshop

During the workshop, we'll review the different Processing code examples and do small exercises.

## Communication

#### Sketches: **`wsserver1`** and  **`wsclient1`**

* websockets
* sending and receiving messages
* creating a protocol
* `ConcurrentLinkedQueue` to synchronize drawing thread and websocket thread


#### Exercise

Create a server message that tells the client to set the fill colour to a random colour whenever a key is pressed.

1. In `keyPressed`, change the string in the server's `sendMessage` method to be three comma-delimited numbers. (hint, look at `mouseDragged` in the client to format the string)
2. Parse the message in the client's `webSocketEvent` and use the three numbers to set a global fill variable used by the client. (hint, look at the `Message` constructor in the server for parsing technique)


#### Experiments

Try running two clients and see what happens. To do this, you'll have to duplicate the `wsclient1` script so you can run two clients. 

#### Sketches: **`wsserver2`** and  **`wsclient2`**

* expanded protocol to add id and event for client messages
* HashMap to tack state of each client
* InputState class to store client state

#### Experiments

We'll setup a local network and try to all connect and draw at the same time.

#### Visualization

We'll go through the different visualizations.

* good code practices: using classes for different visualizations


## Exercise

Work with a partner to brainstorm a mouse-based performance using the code framework. Think of a simple idea, if time allows, we'll try and implement it together.

# 








 




