import websockets.*;

import java.util.concurrent.ConcurrentLinkedQueue;

WebsocketClient wsc;

void setup() {
  size(400, 400);

  //wsc = new WebsocketClient(this, "ws://192.168.1.111:3001");
  wsc = new WebsocketClient(this, "ws://localhost:3001");
}

void draw() {
  // nothing here
}


void mouseDragged() {
  wsc.sendMessage(mouseX + "," + mouseY);
  while (!q.isEmpty()) {
    // pull off message from tail and paint it as ellipse
    Message m = q.poll();
    fill(m.x,m.y,m.z);
    
  }
  
  ellipse(mouseX, mouseY, 10, 10);
}

class Message {

  Message(String msg) {
    String[] t = msg.split(",");
    x = int(t[0]);
    y = int(t[1]);
    z = int(t[2]);
  }

  String toString() {
    return x + "," + y + "," + z;
  }

  int x;
  int y;
  int z;
}

ConcurrentLinkedQueue<Message> q = new ConcurrentLinkedQueue<Message>();

// received message from server
void webSocketEvent(String msg) {
  Message m = new Message(msg);
  println(msg);
  
  q.offer(m);
  
}