class Attractor {
  // position
  float x=0, y=0; 

  // radius of impact
  float radius = 200;


  Attractor(float theX, float theY) {
    x = theX;
    y = theY;
  }


  void attract(Agent theAgent) {
    // calculate distance
    float dx = x - theAgent.x;
    float dy = y - theAgent.y;
    float d = mag(dx, dy);

    if (d > 0 && d < radius) {
      // calculate force
      float s = d/radius;
      float f = 1 / pow(s, 0.5) - 1;
      f = f / radius;
      
      // apply force to node velocity
      theAgent.x += dx * f*2;
      theAgent.y += dy * f*2;
    }
  }

}