import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import java.util.Calendar; 
import generativedesign.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class agentstarter extends PApplet {

/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

//attractor
Attractor myAttractor;

// add your agent parameters here
float param = 2;

float opacity = 60;
float baseHue = 0;

public void setup() {
  //size(800, 600);
  

  agentsCount = height / 3;

  // setup the simple Gui
  gui = new Gui(this);

  gui.addSlider("agentsCount", 10, height);
  gui.addSlider("maxStepSize", 0, 5);
  gui.addSlider("opacity", 0, 255);
  createAgents();
  myAttractor = new Attractor(0,0);
}

public void createAgents() {

  background(255);
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  for (float x = 100; x < width - 100; x += 20)
    for (float y = 100; y < height - 100; y += 20) {
      
        Agent a = new Agent(x, y);
        agents.add(a);
    }
}

public void draw() {
  myAttractor.x = mouseX;
  myAttractor.y = mouseY;
  
  // update all agents
  // draw all the agents
  for (Agent a : agents) {
    if(mousePressed){
      myAttractor.attract(a);
    }
    a.update();
  }
  
  //for (int i = 0; i < agents.size(); i++) {
  //    for (int j = i + 1; j < agents.size(); j++) {

  //      Agent a = agents.get(i);
  //      Agent b = agents.get(j);

  //      float threshold = a.weight / 2 + b.weight / 2;
  //      float d = dist(a.x, a.y, b.x, b.y);
  //      if (d < threshold) {
  //        if (a.weight > b.weight) {
  //          a.weight += b.weight;
  //          b.reset();
  //        } else {
  //          b.weight += a.weight;
  //          a.reset();
  //        }
  //      }
  //    }
  //  }
    
  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }

  // draw Gui last
  gui.draw();

  // interactively adjust agent parameters
  //param = map(mouseX, 0, width, 0, 10);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
public void keyPressed() {
  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

// call back from Gui
public void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}

//void mouseDragged() {
//  Agent a = new Agent(mouseX, mouseY);
//  agents.add(a);
//}

class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  
  PVector velocity;
  float shade;
  float weight;
  float hue;
  int maxWeight =100;

  // create agent that picks starting position itself
  Agent() {
    reset();
  }
  
  public void reset(){
    // random starting position
    int m = 100; // margin
    x = random(m, width - m);
    y = random(m, height - m);
    // pick a random grey shade
    shade = 255 * PApplet.parseInt(random(0, 2));
    // pick random stroke weight
    weight = random(1, maxWeight);
    //hue = (baseHue + random(0,60))%360;
  }

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  public void update() {
    // save last position
    px = x;
    py = y;
    
    float disX = abs(width/2-x);
    float disY = abs(height/2-y);
    float dis = disX+disY;
    
    dis = map(dis,0,width/2,0,3);

    // pick a new position
    x = x + random(-dis, dis);
    y = y + random(-dis, dis);
  }

  public void draw() {

    // draw a line between last position
    // and current position
    strokeWeight(weight);
    stroke(0,0, shade, opacity);
    //rect(x, y, 1, 1);
    line(px, py, x, y);
  }
}
/* 
 * Simple GUI for parameter adjustment
 */







class Gui {

  ControlP5 controlP5;

  ControlGroup ctrl;
  boolean showGUI = false;

  Gui(processing.core.PApplet sketch) {

    int activeColor = color(0, 130, 164);
    controlP5 = new ControlP5(sketch);
    //controlP5.setAutoDraw(false);
    controlP5.setColorActive(activeColor);
    controlP5.setColorBackground(color(170));
    controlP5.setColorForeground(color(50));
    controlP5.setColorCaptionLabel(color(50));
    controlP5.setColorValueLabel(color(255));

    ctrl = controlP5.addGroup("menu", left, 15, 35);
    ctrl.setColorLabel(color(255));

    // position of controls relative to menu toggle
    lastLine = 10;
    ctrl.close();
  }

  int lastLine;

  int left = 5;
  int lineHeight = 15;
  int padding = 5;
  

  public void newLine() {
    lastLine += lineHeight + padding;
  }

  public void addSpace() {
    lastLine += lineHeight/2 + padding;
  }

  public void addSlider(String variableName, float minVal, float maxVal) {
    Slider s = controlP5.addSlider(variableName, minVal, maxVal, left, lastLine, 300, lineHeight);

    s.setGroup(ctrl);
    //s.getCaptionLabel().toUpperCase(true);
    s.getCaptionLabel().getStyle().padding(4, 3, 3, 3);
    s.getCaptionLabel().getStyle().marginTop = -4;
    s.getCaptionLabel().getStyle().marginLeft = 0;
    s.getCaptionLabel().getStyle().marginRight = -14;
    s.getCaptionLabel().setColorBackground(0x99ffffff);

    newLine();
  }

  public void draw() {
    controlP5.show();
    controlP5.draw();
  }


  public void keyPressed() {

    if (key=='m' || key=='M') {
      showGUI = controlP5.getGroup("menu").isOpen();
      showGUI = !showGUI;
    }
    if (showGUI) { 
      controlP5.getGroup("menu").open();
      controlP5.getGroup("menu").show();
    } else {
      controlP5.getGroup("menu").close();
      if (key == 'M') 
        controlP5.getGroup("menu").hide();
    }
    
    // allow saving the frame too
    if (key=='s' || key=='S') {
      String t = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance()); 
      String fn = t + ".png";
      println("saving frame as '" + fn + "'");
      saveFrame(fn);
    }
    
  }
}
class Attractor {
  // position
  float x=0, y=0; 

  // radius of impact
  float radius = 200;


  Attractor(float theX, float theY) {
    x = theX;
    y = theY;
  }


  public void attract(Agent theAgent) {
    // calculate distance
    float dx = x - theAgent.x;
    float dy = y - theAgent.y;
    float d = mag(dx, dy);

    if (d > 0 && d < radius) {
      // calculate force
      float s = d/radius;
      float f = 1 / pow(s, 0.5f) - 1;
      f = f / radius;
      
      // apply force to node velocity
      theAgent.x += dx * f*2;
      theAgent.y += dy * f*2;
    }
  }

}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "agentstarter" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
