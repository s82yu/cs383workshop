
class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  
  PVector velocity;
  float shade;
  float weight;
  float hue;
  int maxWeight =100;

  // create agent that picks starting position itself
  Agent() {
    reset();
  }
  
  void reset(){
    // random starting position
    int m = 100; // margin
    x = random(m, width - m);
    y = random(m, height - m);
    // pick a random grey shade
    shade = 255 * int(random(0, 2));
    // pick random stroke weight
    weight = random(1, maxWeight);
    //hue = (baseHue + random(0,60))%360;
  }

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  void update() {
    // save last position
    px = x;
    py = y;
    
    float disX = abs(width/2-x);
    float disY = abs(height/2-y);
    float dis = disX+disY;
    
    dis = map(dis,0,width/2,0,3);

    // pick a new position
    x = x + random(-dis, dis);
    y = y + random(-dis, dis);
  }

  void draw() {

    // draw a line between last position
    // and current position
    strokeWeight(weight);
    stroke(0,0, shade, opacity);
    //rect(x, y, 1, 1);
    line(px, py, x, y);
  }
}