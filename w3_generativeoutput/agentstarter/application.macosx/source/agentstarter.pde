/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

//attractor
Attractor myAttractor;

// add your agent parameters here
float param = 2;

float opacity = 60;
float baseHue = 0;

void setup() {
  //size(800, 600);
  fullScreen();

  agentsCount = height / 3;

  // setup the simple Gui
  gui = new Gui(this);

  gui.addSlider("agentsCount", 10, height);
  gui.addSlider("maxStepSize", 0, 5);
  gui.addSlider("opacity", 0, 255);
  createAgents();
  myAttractor = new Attractor(0,0);
}

void createAgents() {

  background(255);
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  for (float x = 100; x < width - 100; x += 20)
    for (float y = 100; y < height - 100; y += 20) {
      
        Agent a = new Agent(x, y);
        agents.add(a);
    }
}

void draw() {
  myAttractor.x = mouseX;
  myAttractor.y = mouseY;
  
  // update all agents
  // draw all the agents
  for (Agent a : agents) {
    if(mousePressed){
      myAttractor.attract(a);
    }
    a.update();
  }
  
  //for (int i = 0; i < agents.size(); i++) {
  //    for (int j = i + 1; j < agents.size(); j++) {

  //      Agent a = agents.get(i);
  //      Agent b = agents.get(j);

  //      float threshold = a.weight / 2 + b.weight / 2;
  //      float d = dist(a.x, a.y, b.x, b.y);
  //      if (d < threshold) {
  //        if (a.weight > b.weight) {
  //          a.weight += b.weight;
  //          b.reset();
  //        } else {
  //          b.weight += a.weight;
  //          a.reset();
  //        }
  //      }
  //    }
  //  }
    
  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }

  // draw Gui last
  gui.draw();

  // interactively adjust agent parameters
  //param = map(mouseX, 0, width, 0, 10);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}

//void mouseDragged() {
//  Agent a = new Agent(mouseX, mouseY);
//  agents.add(a);
//}