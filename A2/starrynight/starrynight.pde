/*
 * Changing size and position of circles in a grid
 * - closely based on `P_2_1_2_01` from the book "Generative Design"
 *
 */

Gui gui;

// global variables for agents
float xfactor = 1;
float yfactor = 1;
//int drawMode = 1;
//int tiles = 10; // grid size is tiles by tiles
float maxStep = 0.2;
float probTurn = 0.02;
float strokeSize=10;
PImage img;
float scaleX;
float scaleY;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

//float coin;

void setup() {
  //size should be multiple of img width and height
  //size(750, 375); 
  fullScreen();
  
  background(0);
  agentsCount = 101;
  
  gui = new Gui(this);

  gui.addSlider("strokeSize", 1, 20);
  gui.addSlider("maxStep", 0.2, 2);
  

  img = loadImage("starrynight.jpg");
  scaleX = width/img.width;
  scaleY = height/img.height;
  
  createAgents();
}

// create the grid of agents, one agent per grid location
void createAgents() {

  agents = new ArrayList<Agent>();
  //sAgents = new ArrayList<secretAgent>();
  
  for(int i = 0;i < agentsCount;i++){
    int px = (int)random(0,img.width);
    int py = (int)random(0,img.height);
    Agent a = new Agent(px*scaleX,py*scaleY,px,py);
    agents.add(a);
  }
}

void draw() {
  pushMatrix();
  
  int blendedCount=0;
  
  for(Agent a: agents){
    if(!a.blended && blendedCount == agentsCount-1){
      a.isVincent = true;
      a.opacity=255;
    }
    blendedCount++;
  }
  
  for (Agent a : agents) {

    a.update();

    a.draw();
  }
  
  for (int i = 0; i < agents.size(); i++) {
      for (int j = i + 1; j < agents.size(); j++) {

        Agent a = agents.get(i);
        Agent b = agents.get(j);

        float threshold = max(strokeSize,blendedCount/100);
        float d = dist(a.x, a.y, b.x, b.y);
        //coin = random(0,1);
        if (d < threshold && !a.blended && !b.blended) {
          a.pixelColour = lerpColor(a.pixelColour, b.pixelColour,0.5);
          a.blendedTimes++;
          b.reset();
          
        }
      }
    }
  
  popMatrix();
  
}

void keyPressed() {
  gui.keyPressed();
  
  if (key == ' ') {
    background(0);
    createAgents();
  }
}