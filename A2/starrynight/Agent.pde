
class Agent {

  // location of agent centre
  float x;
  float y;
  
  // pixel location in image
  int px;
  int py;
  
  float l = 1;
  float t;
  int opacity=255;
  float weight;
  color pixelColour;
  boolean blended;
  int blendedTimes;
  
  boolean isVincent;
  
  

  // create the agent
  Agent(float _x, float _y, int _px, int _py) {
    x = _x;
    y = _y;
    px = _px;
    py = _py;
    t = random(TWO_PI);
    weight = strokeSize;
    pixelColour = img.pixels[round(random(0,img.height*img.width))];
    
    isVincent = false;
  }

  void update() {
    
  
    px = round(x/scaleX);
    py = round(y/scaleY);
    
    // get current color
    if(blendedTimes > 3){
      blended = true;
      pixelColour = img.pixels[py * img.width + px];//img.pixels[round(random(0,img.height*img.width))];
      
    }
    if(!blended && !isVincent){
      opacity--;
    }
    
    float oldx = x;
    float oldy = y;

    l += random(-maxStep, maxStep);
    x = x + l * cos(t);
    y = y + l * sin(t);

    t += probTurn;
    
    strokeWeight(weight);
    stroke(pixelColour,opacity);
    line(oldx, oldy, x, y);

    // reset the agent if it leaves the canvas
    if (x < 0 || x > width - 1 || y < 0 || y > height - 1) {
      turnback();
    }

    if(opacity <= 10){
      reset();
    }
  }

void turnback() {
    
    x = x - l * cos(t);
    y = y - l * sin(t);
    l = 1;
    t = random(TWO_PI);

  }
  
void reset() {
    
    x = (int)random(0, width);
    y = (int)random(0, height);
    px = round(x/scaleX);
    py = round(y/scaleY);
    l = 1;
    weight = strokeSize;
    opacity = 255;
    t = random(TWO_PI);
    
  }

  
  void draw() {
      
  }
}